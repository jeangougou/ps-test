'use strict';
const os = require('os')
const readEnvFromPid = require('./readEnvFromPid')
const cl = console.log

const drop = function (array, marker) {
  return array.filter(function (element) {
    return (element != marker)
  })
}

const asPsResult = function (list) {
  const pid = list[0]
  var r = {}
  r[pid] = {
    ppid: list[1],
    name: list[2],
    cmdline: list.slice(3, list.length).join(" ") || false,
    environment: readEnvFromPid(list[0])
  }
  return r
}

module.exports = function (psinput) {
  var lines = psinput.split(os.EOL)
  var fields = lines.map(function (line) {
    var splitLines = line.trim().split(' ')
    var skipEmpty = drop(splitLines, "")
    return asPsResult(skipEmpty)
  })
  return fields
}