// content of index.js
const http = require('http')
const port = 8080
const ps = require('./ps')
const parsePs = require('./parse')
const cl = console.log

const removePid = function (allProcesses, pid) {
  var temp = Object.assign({}, allProcesses)
  delete temp[pid]
  return temp
}

const router = {
  handlers: {
    '/': function (req, res) {
      res.end(JSON.stringify({ "available-url": "/ps" }));
    },
    '/ps': function (req, res) {
      ps(function (err, psOut) {
        const allProcesses = parsePs(psOut)
        const withoutCurrentPid = removePid(allProcesses, process.pid)
        res.end(JSON.stringify(withoutCurrentPid));
      })
    }
  }
}

const requestHandler = function (req, res) {
  console.log(req.url)
  res.writeHead(200, { "Content-Type": "application/json" });
  handler = router.handlers[req.url]
  if ('function' != typeof handler)
    handler = router.handlers['/']
  else
    handler(req, res)
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log(`server is listening on ${port} with pid ${process.pid}`)
})