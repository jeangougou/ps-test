'use strict';

var exec = require('child_process').exec;
module.exports = function (cb) {
  exec('ps -A -o pid,ppid,comm,command --no-headers', function (err, stdout) {
    if (err) {
      cb('Command `ps` returned an error!');
    } else {
      cb(null, stdout);
    }
  });
};