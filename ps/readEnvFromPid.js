'use strict';
const fs = require('fs')

module.exports = function (pid) {
    try {
        return fs.readFileSync('/proc/' + pid + '/environ', 'utf8')
    }
    catch (e) {
        return ""
    }
}