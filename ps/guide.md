# ps-rest

A rest service which provides a `/ps` endpoint that returns the name,
ppid, environment and cmdline or all running processes in JSON (there is
 an example of the expected format at the end of this document).


### Additional Requirements

  - The output should not include details of the service you have written.
  - If there is no cmdline, the cmdline should be set to `false`.
  - The environment should be a list of all environment entries.
  - You may use whichever language you prefer and it's standard libraries.  Shelling out is prohibited.
  - You should be able to cleanly interrupt your service.
  - The service should be runnable as an unprivileged user on CentOS release 7 (x86_64) without any modifications.


### Example Output

```
$ curl localhost:8080/ps
{
    "1305": {
        "cmdline": "-bash",
        "ppid": 1254,
        "name": "bash",
        "environment": [
            "USER=username",
            "LOGNAME=username",
            "HOME=/home/username",
            "PATH=/bin:/usr/bin:/usr/sbin",
            "MAIL=/var/mail/username",
            "SHELL=/usr/bin/zsh",
            "TERM=xterm-256color",
            "LANG=en_US.UTF-8",
        ]
    },
    "843": {
        "cmdline": "/usr/sbin/sshd -D",
        "ppid": 1,
        "name": "sshd",
        "environment": []
    },
    "27": {
        "cmdline": false,
        "ppid": 2,
        "name": "root",
        "environment": []
    },
...
```

